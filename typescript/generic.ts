const arrayOfNum: Array<number> = [1, 2, 3, 4];
const arrayOfStr: Array<string> = ['1', '2', '3'];

//щоб працювало до всіх типів ми дописуємо generic тип
function reverse<T>(array: T[]): T[] {
  return array.reverse();
}

reverse(arrayOfNum);
reverse(arrayOfStr);
