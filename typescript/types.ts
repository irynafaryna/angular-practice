const str: string = 'hello';

const array: number[] = [1, 2, 3];
const array2: Array<number> = [1, 2, 3];

//Tuple
const contact: [string, number] = ['Iryna', 123456];

//Any
let variable: any = 42;
variable = 'new string';
variable = [];

// function
// якщо нічого не повертає функція то пишемо void
function sayMyName(name: string): void {
  console.log(name);
}

sayMyName('Iryna');

//Never
function throwError(message: string): never {
  throw new Error(message);
}

function infinite(): never {
  while (true) {}
}

//Type
type Login = string;
const login: Login = 'admin';
//const login2: Login = 2; // error

type ID = string | number;
const id: ID = 1234;
const id2: ID = '1234';

type SomeType = string | null | undefined;
