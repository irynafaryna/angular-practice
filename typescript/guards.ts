function strip(x: string | number) {
  if (typeof x === 'number') {
    return x.toFixed(2);
  }
  return x.trim();
}

class MyResponse {
  header = 'response header';
  result = 'response result';
}

class MyRrror {
  header = 'error header';
  message = 'error message';
}

function handle(res: MyResponse | MyRrror) {
  if (res instanceof MyResponse) {
    return {
      info: res.header + res.result,
    };
  } else {
    return {
      info: res.header + res.message,
    };
  }
}

// -------------------
type AlertType = 'success' | 'danger' | 'warning';

function setAlertType(type: AlertType) {
  //...
}
setAlertType('success');
setAlertType('warning');
//setAlertType('default')
