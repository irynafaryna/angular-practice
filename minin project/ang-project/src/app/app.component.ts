import { Observable, tap } from 'rxjs';
import { ProductsService } from './services/products.service';
import { IProduct } from './models/product';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'ang-project';
  products$: Observable<IProduct[]>;
  // products: IProduct[] = [];

  loading = false;
  constructor(private ProductsService: ProductsService) {}
  term: '';

  ngOnInit(): void {
    this.loading = true;
    this.products$ = this.ProductsService.getAll().pipe(
      tap(() => (this.loading = false))
    );
    // this.ProductsService.getAll().subscribe((products) => {
    //   this.products = products;
    //   this.loading = false;
    // });
  }
}
